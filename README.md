# Number to Word Converter

This project is an assessment task, which converts numbers from 1 to 1000 and more into English words

## How to build the project?
    npm install && npm run build

## How to run the tests?
    npm test
    
## How to run the project?
    npm start