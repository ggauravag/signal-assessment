import {BritishEnglishWordFromNumberConverter} from "./component/british-english-converter";

export const Dialect = {
    BRITISH_ENGLISH: 1
    // add more for future
};

export function getConverter(dialect) {
    if (dialect === Dialect.BRITISH_ENGLISH) {
        return new BritishEnglishWordFromNumberConverter();
    }
}