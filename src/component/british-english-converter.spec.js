import {expect} from 'chai'
import {BritishEnglishWordFromNumberConverter} from "./british-english-converter";

describe('British English Converter Test', () => {
    const converter = new BritishEnglishWordFromNumberConverter();

    it('should return correct word for 0', () => {
        const actual = converter.convert(0);
        expect(actual).to.equal('Zero');
    });

    it('should return correct word for 1', () => {
        const actual = converter.convert(1);
        expect(actual).to.equal('One');
    });

    it('should return correct word for 10', () => {
        const actual = converter.convert(10);
        expect(actual).to.equal('Ten');
    });

    it('should return correct word for 29', () => {
        const actual = converter.convert(29);
        expect(actual).to.equal('Twenty-nine');
    });

    it('should return correct word for 100', () => {
        const actual = converter.convert(100);
        expect(actual).to.equal('One hundred');
    });

    it('should return correct word for 301', () => {
        const actual = converter.convert(301);
        expect(actual).to.equal('Three hundred one');
    });

    it('should return correct word for 527', () => {
        const actual = converter.convert(527);
        expect(actual).to.equal('Five hundred twenty-seven');
    });

    it('should return correct word for 640', () => {
        const actual = converter.convert(640);
        expect(actual).to.equal('Six hundred forty');
    });

    it('should return correct word for 666', () => {
        const actual = converter.convert(666);
        expect(actual).to.equal('Six hundred sixty-six');
    });

    it('should return correct word for 999', () => {
        const actual = converter.convert(999);
        expect(actual).to.equal('Nine hundred ninety-nine');
    });

    it('should return correct word for 1000', () => {
        const actual = converter.convert(1000);
        expect(actual).to.equal('One thousand');
    });

    it('should return correct word for 1646', () => {
        const actual = converter.convert(1646);
        expect(actual).to.equal('One thousand six hundred forty-six');
    });

    it('should return correct word for 12,465', () => {
        const actual = converter.convert(12465);
        expect(actual).to.equal('Twelve thousand four hundred sixty-five');
    });

    it('should return correct word for 9,05,000', () => {
        const actual = converter.convert(905e3);
        expect(actual).to.equal('Nine hundred five thousand');
    });

    it('should return correct word for 572,056,000', () => {
        const actual = converter.convert(572056e3);
        expect(actual).to.equal('Five hundred seventy-two million fifty-six thousand');
    });

    it('should return correct word for 4,572,056,000', () => {
        const actual = converter.convert(4572056e3);
        expect(actual).to.equal('Four billion five hundred seventy-two million fifty-six thousand');
    });

});