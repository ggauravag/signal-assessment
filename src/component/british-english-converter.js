import {EnglishWordFromNumberConverter} from "./english-converter";

export class BritishEnglishWordFromNumberConverter extends EnglishWordFromNumberConverter {
    constructor() {
        super({
            3: 'thousand',
            6: 'million',
            9: 'billion',
            12: 'trillion'
        });
    }
}