const SPACE_SEPARATOR = ' ';

export class EnglishWordFromNumberConverter {

    constructor(thousandPowerNotation) {
        this.thousandPowerNotation = thousandPowerNotation;
    }

    convert(number) {
        const words = [];
        let tensPower = 0;
        if (number === 0) {
            return "Zero";
        }

        while (number > 0) {
            const remainder = number % 1000;
            if (remainder > 0) {
                let numberInWord = convertNumberUnder1000(remainder);
                let notation = this.thousandPowerNotation[tensPower];
                if (notation) {
                    numberInWord += (" " + notation);
                }
                words.unshift(numberInWord);
            }
            number = Math.trunc(number / 1000);
            tensPower += 3;
        }

        return capitalize(words.join(SPACE_SEPARATOR));
    }

}

function capitalize(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

function convertNumberUnder1000(number) {
    if (number >= 1000) {
        throw Error("Number cannot be greater than or equals to 1000, supplied number: " + number);
    }

    const words = [];
    while (number > 0) {
        if (number >= 100) {
            words.push(NUMERALS[Math.trunc(number / 100)] + " hundred");
            number %= 100;
        } else if (number >= 21) {
            const truncatedTensPlace = Math.trunc(number / 10) * 10; // converts for ex. 87 => 80, 62 => 60
            number %= 10;

            // If a number is in the range 21 to 99, and the second digit is not zero,
            // the number is typically written as two words separated by hyphen
            let numeral;
            if (number > 0) {
                numeral = NUMERALS[truncatedTensPlace] + "-" + NUMERALS[number];
            } else {
                numeral = NUMERALS[truncatedTensPlace];
            }
            words.push(numeral);
            break;
        } else {
            words.push(NUMERALS[number]);
            break;
        }
    }

    return words.join(SPACE_SEPARATOR);
}

function getNumeralOrThrow(number) {
    const numeral = NUMERALS[number];
    if (!numeral) {
        throw Error("No numeral exists for number: " + number);
    }

    return numeral;
}

const NUMERALS = {
    0: "zero",
    1: "one",
    2: "two",
    3: "three",
    4: "four",
    5: "five",
    6: "six",
    7: "seven",
    8: "eight",
    9: "nine",
    10: "ten",
    11: "eleven",
    12: "twelve",
    13: "thirteen",
    14: "fourteen",
    15: "fifteen",
    16: "sixteen",
    17: "seventeen",
    18: "eighteen",
    19: "nineteen",
    20: "twenty",
    30: "thirty",
    40: "forty",
    50: "fifty",
    60: "sixty",
    70: "seventy",
    80: "eighty",
    90: "ninety"
};