import {Dialect, getConverter} from "./converter-factory";
import {expect} from 'chai';
import {BritishEnglishWordFromNumberConverter} from "./component/british-english-converter";

describe('Converter Factory Test', () => {
    it('should create British English Converter', () => {
        const actualConverter = getConverter(Dialect.BRITISH_ENGLISH);
        expect(actualConverter).to.instanceOf(BritishEnglishWordFromNumberConverter);
    });
});