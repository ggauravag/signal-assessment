#!/usr/bin/env node
'use strict';

import {Dialect, getConverter} from "./converter-factory";
import {question, questionInt} from 'readline-sync'

const wordFromNumberConverter = getConverter(Dialect.BRITISH_ENGLISH);

console.log('##### Welcome to English Number to Word Converter #####');
while (true) {
    const number = questionInt('Enter a number to convert to word: ');
    console.log('==> ' + wordFromNumberConverter.convert(number));

    const choice = question('Enter "exit" to stop or any other key to continue!\n');
    if (choice === 'exit') {
        break;
    }
}
console.log('##### Thank you #####');
